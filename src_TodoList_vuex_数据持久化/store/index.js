// 1.安装
// 2.导包
import Vue from 'vue'
import Vuex from 'vuex'
import { nanoid } from 'nanoid'
Vue.use(Vuex)

// 5.创建4大核心: state,mutations,actions,getters
const state = {
    // 列表数据
    todoList: [

    ]
};
const mutations = {
    // 新增
    ADD(state, todo) {
        state.todoList.push(todo)
    },
    // 修改单个选中框状态
    CHANGEDONE(state, { id, done }) {
        state.todoList.map(item => {
            if (item.id === id) {
                item.done = done
            }
            return item
        })
    },
    // 删除单条数据
    DEL(state, id) {
        state.todoList = state.todoList.filter(item => item.id !== id)
    },
    // 全选按钮
    CHANGECKECKALL(state, checkAllDone) {
        state.todoList.map(item => {
            if (item.done !== checkAllDone) {
                item.done = checkAllDone
            }
            return item
        })
    },
    // 清除所有已完成
    DELALLFINISH(state) {
        state.todoList = state.todoList.filter(item => !item.done)
    },
    // 初始化
    INIT(state, list) {
        console.log(list);
        state.todoList = list;
    }
};
const actions = {
    // 新增
    addTodo({ commit }, todo) {
        commit('ADD', todo)
    },
    // 修改单个选中框状态
    changeDone({ commit }, todoDone) {
        commit('CHANGEDONE', todoDone)
    },
    // 删除单个数据
    delTodo({ commit }, id) {
        commit('DEL', id)
    },
    // 全选按钮
    changeCheckAllDone({ commit }, checkAllDone) {
        commit('CHANGECKECKALL', checkAllDone)
    },
    // 清除所有已完成
    delAllFinish({ commit }) {
        commit('DELALLFINISH')
    },
    // 初始化
    init({ commit }, list) {
        console.log(list);
        commit('INIT', list)
    }
};
const getters = {
    // 完成的个数
    finishNum(state) {
        return state.todoList.reduce((prev, val) => {
            return prev + val.done;
        }, 0);
    },
    // 全选按钮状态
    checkAllFlag(state) {
        return state.todoList.every((item) => item.done) && state.todoList.length;
    },
};

// 3.创建并暴露store
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
})

// 4.在创建vm文件中导入并在配置对象中使用store