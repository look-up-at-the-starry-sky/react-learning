import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  beforeCreate() {
    Vue.prototype.$bus = this
  },
  render: h => h(App),
}).$mount('#app')

// public 和 assets 都是静态文件
// 差别是 assets 中的内容会被webpack处理,而public中的静态资源不会被webpack处理
